//
// Created by david on 8/4/19.
//

#include "unity.h"     // compile/link in Unity test framework
#include "app-events.h"
#include "waggle.h"
#include "mock_waggle-host.h"

static wag_event_t s_cb_event;
static uint64_t s_time;

void setUp(void)
{
  s_cb_event = EVENT_INVALID;
  s_time = 0;
}

void tearDown(void)
{

}

void test_init(void)
{
  TEST_ASSERT_EQUAL(WAGGLE_ERR_OK, waggle_init());
}

void callback(wag_event_t event, uint64_t time)
{
  s_cb_event = event;
  s_time = time;
  return;
}

void test_subscribe_argChecks(void)
{
  waggle_sub_t subscription;
  TEST_ASSERT_EQUAL(WAGGLE_ERR_NULLPTR, waggle_subscribe(EVENT_TICK, 0, callback));
  TEST_ASSERT_EQUAL(WAGGLE_ERR_NULLPTR, waggle_subscribe(EVENT_TICK, &subscription, 0));
  TEST_ASSERT_EQUAL(WAGGLE_ERR_INVEVT, waggle_subscribe(EVENT_INVALID, &subscription, callback));
  TEST_ASSERT_EQUAL(WAGGLE_ERR_INVEVT, waggle_subscribe(EVENT_INVALID - 1, &subscription, callback));
  TEST_ASSERT_EQUAL(WAGGLE_ERR_INVEVT, waggle_subscribe(EVENT_COUNT, &subscription, callback));
  TEST_ASSERT_EQUAL(WAGGLE_ERR_INVEVT, waggle_subscribe(EVENT_COUNT + 1, &subscription, callback));
  TEST_ASSERT_EQUAL(WAGGLE_ERR_OK, waggle_subscribe(EVENT_TICK, &subscription, callback));
}

void test_subscription_operation(void)
{
  // Init
  TEST_ASSERT_EQUAL(WAGGLE_ERR_OK, waggle_init());

  // Subscribe
  waggle_sub_t subscription;
  TEST_ASSERT_EQUAL(WAGGLE_ERR_OK, waggle_subscribe(EVENT_TICK, &subscription, callback));

  // Notify event
  wag_enterCriticalSection_ExpectAndReturn(true);
  wag_getSystemTimeMicroseconds_ExpectAndReturn(1);
  wag_exitCriticalSection_ExpectAndReturn(true);
  TEST_ASSERT_EQUAL(WAGGLE_ERR_OK, waggle_notifyEvent(EVENT_TICK));

  // Receive event
  wag_getSystemTimeMicroseconds_ExpectAndReturn(2);
  TEST_ASSERT_EQUAL(WAGGLE_ERR_OK, waggle_process_events());
  TEST_ASSERT_EQUAL(EVENT_TICK, s_cb_event);
  TEST_ASSERT_EQUAL(1, s_time);

  // Test no events pending
  s_cb_event = EVENT_INVALID;
  s_time = 4242;
  wag_getSystemTimeMicroseconds_ExpectAndReturn(3);
  TEST_ASSERT_EQUAL(WAGGLE_ERR_OK, waggle_process_events());
  TEST_ASSERT_EQUAL(EVENT_INVALID, s_cb_event);
  TEST_ASSERT_EQUAL(4242, s_time);
}