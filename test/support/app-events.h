//
// Created by david on 8/4/19.
//

#ifndef WAGGLE_APP_EVENTS_H
#define WAGGLE_APP_EVENTS_H

typedef enum app_events
{
    EVENT_INVALID = (-1),   ///< An invalid event code
    EVENT_TICK = 0,         ///< A tick has occurred

    EVENT_COUNT             ///< Required event list terminator

} wag_event_t;

#endif //WAGGLE_APP_EVENTS_H
