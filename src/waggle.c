/**
 * @file waggle.c
 *
 * @brief Implementation of the Waggle embedded event bus
 *
 * @copyright
 * &copy; YYYY Client
 * All rights reserved.
 */

/* ======================= INCLUDES ========================================= */

#include <stdlib.h>
#include <string.h>

#include "waggle.h"
#include "waggle-host.h"
#include "app-events.h"

/* ======================= PRIVATE CONSTANTS ================================ */

/* ======================= PRIVATE DATA TYPES =============================== */

typedef struct waggle_event {

    uint64_t            lastOccurrence;
    uint64_t            lastNotification;
    struct waggle_sub * pFirstSubscription;

} waggle_event_t;

/* ======================= PRIVATE STATE ==================================== */

static waggle_event_t s_eventArray[EVENT_COUNT];

/* ======================= PRIVATE FUNCTION DECLARATIONS ==================== */

/**
 * Check if an event code is valid
 * @param event The event to check
 * @return True if valid, False if invalid
 */
bool wag_valid_event(wag_event_t event);

/* ======================= PUBLIC FUNCTION IMPLEMENTATIONS ================== */

waggle_err_t waggle_init(void)
{
  waggle_err_t result = WAGGLE_ERR_OK;

  for(wag_event_t event = 0; event < EVENT_COUNT; event++)
  {
    s_eventArray[event].lastOccurrence = 0;
    s_eventArray[event].lastNotification = 0;
    s_eventArray[event].pFirstSubscription = NULL;
  }

  return result;
}

/* -------------------------------------------------------------------------- */

void waggle_deInit(void)
{
  memset(s_eventArray, 0x00, sizeof(s_eventArray));
}

/* -------------------------------------------------------------------------- */

waggle_err_t waggle_notifyEvent(wag_event_t event)
{
  waggle_err_t result = WAGGLE_ERR_OK;

  if(event >= EVENT_COUNT)
  {
    result = WAGGLE_ERR_COUNT;
  }
  else
  {
    waggle_event_t * pEvent = &(s_eventArray[event]);

    wag_enterCriticalSection(); /* ========= BEGIN CRITICAL SECTION ========= */

    uint64_t now = wag_getSystemTimeMicroseconds();
    if(now > pEvent->lastOccurrence)
    {
      pEvent->lastOccurrence = now;
    }

    wag_exitCriticalSection();  /* ========== END CRITICAL SECTION ========== */
  }

  return result;
}

/* -------------------------------------------------------------------------- */

waggle_err_t waggle_subscribe(wag_event_t event,
                              waggle_sub_t * pSubscription,
                              waggle_callback_t pCallbackFunction)
{
  if((NULL == pSubscription) || (NULL == pCallbackFunction))
  {
    return WAGGLE_ERR_NULLPTR;
  }
  else if(!wag_valid_event(event))
  {
    return WAGGLE_ERR_INVEVT;
  }
  else
  {
    // Save callback
    pSubscription->callback = pCallbackFunction;

    // This subscription will now be at the end of the linked list
    pSubscription->pNextSubscription = NULL;

    // Add subscription to linked list
    waggle_sub_t * pSub = s_eventArray[event].pFirstSubscription;
    if(NULL == pSub)
    {
      s_eventArray[event].pFirstSubscription = pSubscription;
    }
    else
    {
      while(NULL != pSub->pNextSubscription)
      {
        pSub = pSub->pNextSubscription;
      }
      pSub->pNextSubscription = pSubscription;
    }

  }
  return WAGGLE_ERR_OK;
}

/* -------------------------------------------------------------------------- */

#include <stdio.h>
waggle_err_t waggle_process_events(void)
{
  uint64_t now = wag_getSystemTimeMicroseconds();
  for(wag_event_t event = 0; event < EVENT_COUNT; event++)
  {
    if(s_eventArray[event].lastOccurrence > s_eventArray[event].lastNotification)
    {
      waggle_sub_t * pSub = s_eventArray[event].pFirstSubscription;
      while(NULL != pSub)
      {
        pSub->callback(event, s_eventArray[event].lastOccurrence);
        s_eventArray[event].lastNotification = now;
        pSub = pSub->pNextSubscription;
      }
    }
  }
  return WAGGLE_ERR_OK;
}

/* ======================= PRIVATE FUNCTION IMPLEMENTATIONS ================= */

#include <stdio.h>
bool wag_valid_event(wag_event_t event)
{
  if(event <= EVENT_INVALID)
  {
    return false;
  }
  else if(event >= EVENT_COUNT)
  {
    return false;
  }
  return true;
}

/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */

