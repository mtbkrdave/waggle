/**
 * @file waggle-host.h
 *
 * @brief Required host-dependent functions for Waggle
 *
 * For portability, Waggle requires the functions declared in this header to be
 * implemented by the host system/application.
 *
 * @copyright 2019 David D. Rea <dave@daverea.com>
 * All rights reserved.
 */

#ifndef _WAGGLE_HOST_H
#define _WAGGLE_HOST_H

/* ======================= TYPE INCLUDES ==================================== */

#include <stdbool.h>
#include <stdint.h>

/* ======================= PUBLIC CONSTANTS ================================= */

/* ======================= PUBLIC DATA TYPES ================================ */

/* ======================= PUBLIC FUNCTION DECLARATIONS ===================== */

/**
 * Application-defined function called when entering a critical section. May be
 * used to disable global interrupts.
 * @return true on success, false on failure
 */
bool wag_enterCriticalSection(void);

/* -------------------------------------------------------------------------- */

/**
 * Application-defined function called when exiting a critical section. May be
 * used to enable global interrupts.
 * @return true on success, false on failure
 */
bool wag_exitCriticalSection(void);

/* -------------------------------------------------------------------------- */

/**
 * Application-defined function called to get the current system time in
 * microseconds
 *
 * @note exp(2,32)-1 microseconds rolls over after 1.19 hours
 *       exp(2,64)-1 microseconds rolls over after 584,942 years
 *       Waggle handles time rollovers, but if occurrences of an event can be
 *       separated by more than 1.19 hours, use the 64-bit size.
 *
 * @note Waggle will call this function within a critical section.
 *
 * @return The current system time in microseconds
 */
uint64_t wag_getSystemTimeMicroseconds(void);

#endif /* _WAGGLE_HOST_H */
