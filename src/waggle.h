/**
 * @file waggle.h
 *
 * @brief Public interface to the Waggle embedded event bus
 *
 * Waggle is an event bus designed for use in bare-metal embedded systems. It
 * supports development of decoupled, deterministic embedded applications
 * without introducing the complexity, resource consumption, and potential for
 * blocking and deadlock of an RTOS.
 *
 * @copyright 2019 David D. Rea <dave@daverea.com>
 * All rights reserved.
 */

#ifndef _WAGGLE_H
#define _WAGGLE_H

/* ======================= TYPE INCLUDES ==================================== */

#include <stdint.h>
#include "waggle-host.h"
#include "app-events.h"

/* ======================= PUBLIC CONSTANTS ================================= */

/* ======================= PUBLIC DATA TYPES ================================ */

typedef void (* waggle_callback_t)(wag_event_t event, uint64_t timeOfOccurrence);

typedef enum waggle_err {

  WAGGLE_ERR_OK = 0,    ///< Success
  WAGGLE_ERR_NOIMP,     ///< Not implemented
  WAGGLE_ERR_NULLPTR,   ///< Null pointer error
  WAGGLE_ERR_INVEVT,    ///< Invalid event code

  WAGGLE_ERR_COUNT      /* must be last */

} waggle_err_t;

typedef struct waggle_sub {
  wag_event_t         evt;
  uint64_t            lastFired;
  waggle_callback_t   callback;
  struct waggle_sub * pNextSubscription;

} waggle_sub_t;

/* ======================= PUBLIC FUNCTION DECLARATIONS ===================== */

/**
 * Initialize the Waggle event bus subsystem
 * @param pEvents Pointer to an array of this system's events
 * @param length Number of events in array
 * @return Result code indicating success (0) or failure (>0)
 */
waggle_err_t waggle_init(void);

/* -------------------------------------------------------------------------- */

/**
 * De-initialize the waggle event bus subsystem
 */
void waggle_deInit(void);

/* -------------------------------------------------------------------------- */

/**
 * Publish an event to the event bus
 * @param event The event index (from application enum) to publish
 * @return Result code indicating success (0) or failure (>0)
 */
waggle_err_t waggle_notifyEvent(wag_event_t event);

/* -------------------------------------------------------------------------- */

/**
 * Subscribe to an event in waggle
 * @param event The event index (from application enum) to subscribe to
 * @param pSubscription Pointer to subscriber-allocated subscription struct
 * @param pCallbackFunction Pointer to function to be called when event occurs
 * @return Result code indicating success (0) or failure (>0)
 */
waggle_err_t waggle_subscribe(wag_event_t event,
        waggle_sub_t * pSubscription,
        waggle_callback_t pCallbackFunction);

/* -------------------------------------------------------------------------- */

/**
 * Process event notifications to subscriptions
 * @return Result code indicating success (0) or failure (>0)
 */
waggle_err_t waggle_process_events(void);

#endif /* _WAGGLE_H */
